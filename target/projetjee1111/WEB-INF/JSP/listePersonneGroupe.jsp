<%-- 
    Document   : Liste des Adhérents dans un Groupe donné
    Created on : 31/07/2020 8:00
    Author     : Francis RAHARISON
--%>
<%@page import="java.util.List"%>
<%@page import="controllers.ListePersonneGroupeController"%>
<%@page import="models.Groupe"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Liste Adhérents d'un Groupe</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
              integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" 
              crossorigin="anonymous">
        <script src="https://kit.fontawesome.com/a0c8b86555.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="styleCss/navbarStyle.css" type="text/css">
    </head>
    <body>
        <c:import url="/WEB-INF/JSP/barreDeMenu.jsp"></c:import>
            <!--DEBUT JSTL-->
        <c:if test="${!empty tousLesGroupes}">
            <h2>Afficher les adhérents d'un Groupe</h2>
            <form  method='post' action='ServletFrontController?cmd=page5'>
                <label for=''>Choisir le Groupe</label><br><br>
                <select  name='selecteur' id='selecteur'>
                    <c:forEach items="${tousLesGroupes}" var="tousLesGroupes">  
                        <option value='${tousLesGroupes.idGroupe}' name="idGroupe"> ${tousLesGroupes.nomGroupe} </option>
                    </c:forEach>
                </select>
                <button type='submit'>Valider</button>
            </form>
        </c:if>  
       
            <c:if test="${empty tousLesGroupes}">
            <h4>Les adhérents de ce Groupe</h4>
                    
            <div class="col-md-6">
                <table class="table table-dark ">
                    <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Nom</th>
                            <th scope="col">Prenom</th>
                            <th scope="col">Groupe</th>
                        </tr>
                    </thead>
                    <tbody>
                      
                            <c:forEach items="${tousLesGr}" var="tousLesGr">
                                <tr>
                                    <td>${tousLesGr.id} </td>
                                    <td>${tousLesGr.nom} </td>
                                    <td>${tousLesGr.prenom} </td>
                                    <td>${tousLesGr.persIdGroupe} </td>

                                </tr>
                            </c:forEach>
                     
                    </tbody>
                   
                </table>
                 <a  href="http://localhost:8383/Brouillon2/index.html" >Cliquer ici pour voir les adhérents sans Groupe</a>
            </div>
        </c:if>
            
        <!--FIN JSTL-->

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" 
                integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" 
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
                integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
    </body>
    
</html>
