/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Comparator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *Classe Groupe ou appartient un adhérent
 * @author pc
 */
@Entity
@Table(name="Groupe")
public class Groupe implements Serializable{
   public static Comparator<Groupe> comparatorNom = (Groupe groupe1,Groupe groupe2)
           ->groupe1.getNomGroupe().toUpperCase().compareTo(groupe2.getNomGroupe().toUpperCase());
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idGroupe")
    private Integer idGroupe;
    
    @Column(name="nomGroupe",length = 50,nullable = false,unique = true)
    private String nomGroupe;
    @OneToMany
    @JoinColumn(name="persIdGroupe")
    private List<Personne> personnes = new ArrayList<Personne>();

    public Groupe() {
    }

    public Groupe(Integer idGroupe, String nomGroupe) {
        this.idGroupe = idGroupe;
        this.nomGroupe = nomGroupe;
    }

    @Override
    public String toString() {
        return "Groupe{" + "idGroupe=" + idGroupe + ", nomGroupe=" + nomGroupe + '}';
    }

    public Integer getIdGroupe() {
        return idGroupe;
    }

    public void setIdGroupe(Integer idGroupe) {
        this.idGroupe = idGroupe;
    }

    public String getNomGroupe() {
        return nomGroupe;
    }

    public void setNomGroupe(String nomGroupe) {
        this.nomGroupe = nomGroupe;
    }

    public List<Personne> getPersonnes() {
        return personnes;
    }

    public void setPersonnes(List<Personne> personnes) {
        this.personnes = personnes;
    }
    
    }
