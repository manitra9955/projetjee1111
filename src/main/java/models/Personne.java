/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Comparator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Une bean "Personne"
 *
 * @author Francis RAHARISON
 */
@Entity
@Table(name="Personne")
public class Personne implements Serializable {
    public static Comparator<Personne> comparatorNom = (Personne pers1,Personne pers2)
            ->pers1.getNom().toUpperCase().compareTo(pers2.getNom().toUpperCase());
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @NotBlank(message = "Le non ne peuxt pas être vide")
    @Size(max = 30, message = "Le nom contient pas plus de 30 caractères")
    @Size(min = 2, message = "Le nom contient au moins 2 caractères ")
    @Column(name="Nom",length = 30,unique = false,nullable = false)
    private String nom;

   @NotBlank(message = "Le prénom ne doit pas être vide")
   @Size(max = 20, message = "Le prénom contient pas plus de 20 Caractères")
   @Size(min = 2, message = "Le prénom contient pas plus de 20 Caractères")
   @Column(name="Prenom",length = 20,unique = false,nullable = false)
    private String prenom;
   
   private Integer persIdGroupe;

    public Personne(String nom, String prenom) {
        // this.id = id;
        this.nom = nom;
        this.prenom = prenom;
    }

    public Personne(Integer id, String nom, String prenom) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;

    }

    public Personne() {
    }

//    public Personne(String sonNom, String sonPrenom) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Integer getPersIdGroupe() {
        return persIdGroupe;
    }

    public void setPersIdGroupe(Integer persIdGroupe) {
        this.persIdGroupe = persIdGroupe;
    }

}
