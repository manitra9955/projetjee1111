/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Personne;
/**
 *
 * @author Francis RAHARISON
 */
public interface ICommand {
    
public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception;

}
