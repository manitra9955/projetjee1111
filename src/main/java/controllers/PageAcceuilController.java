/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Utilitaires.CreerListe;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Personne;
import models.Users;

/**
 *
 * @author Francis RAHARISON
 */
public class PageAcceuilController implements ICommand {

    private static List<Personne> personneListe = new ArrayList();
    int pagesVues = 0;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        String date =LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
//      String monCookie;
//      monCookie="Bienvenu " + date;
//      Cookie cookie = new Cookie("monCookie", URLEncoder.encode(monCookie,"UTF-8"));
//      cookie.setMaxAge(60*60*24);
//      response.addCookie(cookie);
//       

        String date = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        String monCookie = "  Today-" + date;
        Cookie cookie = new Cookie("monCookie", URLEncoder.encode(monCookie, "UTF-8"));
        cookie.setMaxAge(60*60*24);
        response.addCookie(cookie);
         String input="admin";
        String mdpCript;
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.reset();
        md.update(input.getBytes());
        mdpCript = String.format("%064x",new BigInteger(1,md.digest()));
        Users user = new Users(null,"admin",mdpCript);
       //  DAOPersonne.DaoPersonne.creatUser(user);
       
//        if (personneListe.isEmpty()) {
//            HttpSession session = request.getSession();
//            //Création de cookie
//
//            session.setAttribute("pagesVues", pagesVues);
//            Personne personne1 = new Personne(1, "Richard", "Pierre");
//            Personne personne2 = new Personne(2, "Delon", "Alain");
//            Personne personne3 = new Personne(3, "Stallone", "Silvester");
//
//            personneListe.add(personne1);
//            personneListe.add(personne2);
//            personneListe.add(personne3);
//        }
//        CreerListe listeP = new CreerListe();
//        listeP.getpersonneListe();
        request.setAttribute("tousLesAdherents", personneListe);

        return "navbar.jsp";

    }

    public static List<Personne> getpersonneListe() {
        return personneListe;
    }

    //   return "navbar.jsp";
}
