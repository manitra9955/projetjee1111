/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOPersonne;

import java.sql.SQLException;
import models.Personne;
import frontController.ServletFrontController;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import models.Users;

/**
 *
 * @author pc
 */
public class DaoPersonne {
    // static ResultSet rst;

    private static  EntityManager entManag = ServletFrontController.entManag;
    private static final Logger LOGGER = Logger.getLogger(DaoPersonne.class.getName());
//    private static final Connection CONNECTION = ServletFrontController.connection; 
    // METHODE POUR INSTANCIER UNE PERSONNE DEPUIS UN ENREGISTREMENT
    //DE LA TABLE PERSONNE DE LA BD

//    public static Personne resultSetPersonne(ResultSet rst) throws SQLException, Exception {
//
//       Personne personne = new Personne();
//       personne.setId(rst.getInt("id"));
//        personne.setNom(rst.getString("nom"));
//       personne.setPrenom(rst.getString("prenom"));
//      
//       return personne;
//    }
    
    
//    FONCTION findPersonneByGroupe
    public static List<Personne> findPersByGroupe(int idPersGroupe) throws SQLException{
       
   
         List desPersonnes=null;
        try{
         
         desPersonnes = entManag.createQuery("SELECT p FROM Personne p WHERE p.persIdGroupe="+idPersGroupe).getResultList();
        // desPersonnes = entManag.createQuery("SELECT p FROM Personne p").getResultList();
         LOGGER.info("Requete marche bien");
        }catch(Exception ex){
         LOGGER.warning("Probleme lecture Personne par groupe ");
        }return desPersonnes;
    }
//    // FONCTION findAll-Pour afficher la table Client de la BD Reverso
    public static List<Personne> findAll() throws SQLException {
        // Statement stmt=null;
        // String query = "SELECT * FROM tbpersonne";
        List desPersonnes = null;
        try {
           
            desPersonnes = entManag.createQuery("SELECT p FROM Personne p").getResultList();
             Collections.sort(desPersonnes,Personne.comparatorNom);
        } catch (Exception ex) {
            LOGGER.severe("Probleme de lecture de la table Personne" + ex.getMessage());
        }
        return desPersonnes;
    }

    public static void savePersonne(Personne personne) throws SQLException {
        EntityTransaction entTrans = entManag.getTransaction();
        // Personne pers=null;
        try {
            entTrans.begin();
            // personne = new Personne();
            if (personne.getId() == null) {
                entManag.persist(personne);
            } else {
                entManag.merge(personne);
            }
            entTrans.commit();
        } catch (Exception ex) {
            LOGGER.warning("Probleme savePersonne" + ex.getMessage());
        }
    }

    public static Personne findByid(int id) throws SQLException {
        Personne pers = null;
        try {
            pers = entManag.find(Personne.class, id);
        } catch (Exception ex) {
            LOGGER.severe("Probleme recherche Personne par id" + ex.getMessage());
        }
        return pers;
    }

    // public static void Personne DeletePers(Personne pers) throws SQLException {
    public static void DeletePersonne(int id) throws SQLException {
        EntityTransaction entTrans = entManag.getTransaction();
        try {
            entTrans.begin();
            entManag.remove(entManag.find(Personne.class, id));
            entTrans.commit();
        } catch (Exception ex) {
            LOGGER.warning("Probleme de suppression personne par id " + ex.getMessage());
        }
    }
     public static void creatUser(Users user) throws SQLException {
        EntityTransaction entTrans = entManag.getTransaction();
        try {
            entTrans.begin();
                entManag.persist(user);
                entManag.merge(user);
                entTrans.commit();
        } catch (Exception ex) {
            LOGGER.warning("Probleme savePersonne" + ex.getMessage());
        }
    //  return PageAcceuilController.getpersonneListe();
}
}