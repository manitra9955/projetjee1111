/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOGroupe;
//
///**
// *
// * @author pc
// */
//public class DaoGroupe {
//    
//}

import java.sql.ResultSet;
import java.sql.SQLException;
import frontController.ServletFrontController;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import models.Groupe;

/**
 *
 * @author pc
 */
public class DaoGroupe {
    // static ResultSet rst;

    private static final EntityManager entManag = ServletFrontController.entManag;
    private static final Logger LOGGER = Logger.getLogger(DaoGroupe.class.getName());
//    private static final Connection CONNECTION = ServletFrontController.connection; 
    // METHODE POUR INSTANCIER UN GROUPE DEPUIS UN ENREGISTREMENT
    //DE LA TABLE PERSONNE DE LA BD

//    public static Groupe resultSetGroupe(ResultSet rst) throws SQLException, Exception {

////        Groupe groupe = new Groupe();
////        groupe.setIdGroupe(rst.getInt("idGroupe"));
////        groupe.setNomGroupe(rst.getString("nomGroupe"));
////        return groupe;
//    }
    //LA FONCTION QUI AFFICHE LA LISTE DES GROUPES

    public static List<Groupe> findAll() throws SQLException {
        // Statement stmt=null;
        // String query = "SELECT * FROM tbpersonne";
        List desGroupes = null;
        try {
            desGroupes = entManag.createQuery("SELECT g FROM Groupe g ").getResultList();

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Probleme de lecture de la table Groupe{0}", ex.getMessage());
        }
        return desGroupes;
    }
//LA FONCTION QUI MODIFIE ET AJOUTE UN GROUPE DANS LA TABLE GROUPE
    public static void saveGroupe(Groupe groupe) throws SQLException {
        EntityTransaction entTrans = entManag.getTransaction();
        // Personne pers=null;
        try {
            entTrans.begin();
            // Groupe groupe = new Groupe();
            if(groupe.getIdGroupe() == null){    
                entManag.persist(groupe);
            } else {
                entManag.merge(groupe);
            }
            entTrans.commit();
        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, "Probleme saveGroupe{0}", ex.getMessage());
        }
    }

    public static Groupe findByid(Integer id) throws SQLException {
        Groupe groupe = null;
        try {
           groupe = entManag.find(Groupe.class, id);
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Probleme recherche Personne par id{0}", ex.getMessage());
        }
        return groupe;
    }

    // public static void Personne DeletePers(Personne pers) throws SQLException {
    public static void DeleteGroupe(int id) throws SQLException {
        EntityTransaction entTrans = entManag.getTransaction();
        try {
            entTrans.begin();
            entManag.remove(entManag.find(Groupe.class, id));
            entTrans.commit();
        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, "Probleme de suppression groupe par id {0}", ex.getMessage());
        }
    }
    //  return PageAcceuilController.getpersonneListe();
}
